(function() {
  'use strict';

  angular
    .module('trax')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();

(function() {
    'use strict';

    angular
        .module('trax')
        .directive('traxSolution', traxSolution);

    /** @ngInject */
    function traxSolution() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/content/solution/solution.html',
            scope: {
                creationDate: '='
            },
            controller: SolutionController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function SolutionController() {
            var vm = this;

            initialize();

            function initialize() {
                vm.init = true;
            }
        }
    }

})();
(function() {
    'use strict';

    angular
        .module('trax')
        .directive('traxList', traxList);

    /** @ngInject */
    function traxList() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/content/list/list.html',
            scope: {
                creationDate: '='
            },
            controller: ListController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function ListController() {
            var vm = this;

            initialize();

            function initialize() {
                vm.init = true;
            }
        }
    }

})();
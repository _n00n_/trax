(function() {
    'use strict';

    angular
        .module('trax')
        .directive('contentComponent', contentComponent);

    /** @ngInject */
    function contentComponent() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/content/content.html',
            scope: {
                creationDate: '='
            },
            controller: ContentController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function ContentController($scope, $rootScope) {

            var vm = this;

            $scope.$watch(function() {
                return $rootScope.selectedTab;
            }, function() {
                vm.selectedTab = $rootScope.selectedTab;
            }, true);

            vm.selectedTab = 0;



        }
    }

})();
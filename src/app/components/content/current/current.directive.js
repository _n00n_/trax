(function() {
    'use strict';

    angular
        .module('trax')
        .directive('traxCurrent', traxCurrent);

    /** @ngInject */
    function traxCurrent() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/content/current/current.html',
            scope: {
                creationDate: '='
            },
            controller: CurrentController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function CurrentController() {
            var vm = this;

            initialize();

            function initialize() {
                vm.init = true;
            }

            vm.test = function test(){
                alert(1234);
            }
        }
    }

})();
(function() {
    'use strict';

    angular
        .module('trax')
        .directive('traxLogo', traxLogo);

    /** @ngInject */
    function traxLogo() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/head/logo/logo.html',
            scope: {
                creationDate: '='
            },
            controller: logoController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function logoController() {
            var vm = this;

            initialize();

            function initialize() {
                vm.init = true;
            }
        }
    }

})();
(function() {
    'use strict';

    angular
        .module('trax')
        .directive('traxInfo', traxInfo);

    /** @ngInject */
    function traxInfo() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/head/info/info.html',
            scope: {
                creationDate: '='
            },
            controller: infoController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function infoController($scope, $mdDialog, $document) {
            var vm = this;

            initialize();

            function initialize() {
                vm.init = true;
            }

            vm.showInfo = function showInfo(ev){
                $mdDialog.show({
                    controller: InfoDialogController,
                    templateUrl: 'app/components/head/info/info.dialog.html',
                    parent: angular.element($document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                    .then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
            };
        }
    }



})();
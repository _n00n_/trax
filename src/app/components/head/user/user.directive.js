(function() {
    'use strict';

    angular
        .module('trax')
        .directive('traxUser', traxUser);

    /** @ngInject */
    function traxUser() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/head/user/user.html',
            scope: {
                creationDate: '='
            },
            controller: userController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function userController($scope, $mdDialog, $document) {
            var vm = this;

            vm.user = {
                name: 'Test User'
            };

            vm.showMenu = function showMenu(ev){
                $mdDialog.show({
                    controller: MenuDialogController,
                    templateUrl: 'app/components/head/user/menu.dialog.html',
                    parent: angular.element($document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                    .then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
            };
        }
    }

})();
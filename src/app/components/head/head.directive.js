(function() {
    'use strict';

    angular
        .module('trax')
        .directive('headComponent', headComponent);

    /** @ngInject */
    function headComponent() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/head/head.html',
            scope: {
                creationDate: '='
            },
            controller: HeadController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function HeadController() {

            var vm = this;

            vm.initialize = function initialize() {
            }

            vm.initialize();
        }
    }

})();
(function() {
    'use strict';

    angular
        .module('trax')
        .directive('bottomComponent', bottomComponent);

    /** @ngInject */
    function bottomComponent() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/bottom/bottom.html',
            scope: {
                creationDate: '='
            },
            controller: BottomController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function BottomController($mdDialog, $scope, $rootScope, $document) {

            var vm = this;

            $rootScope.selectedTab = 0;

            vm.showImpress = function showImpress(ev){
                $mdDialog.show({
                    controller: ImpressDialogController,
                    templateUrl: 'app/components/bottom/impress/impress.dialog.html',
                    parent: angular.element($document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                    .then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
            };

            vm.showInfo = function showInfo(ev){
                $mdDialog.show({
                    controller: InfoDialogController,
                    templateUrl: 'app/components/head/info/info.dialog.html',
                    parent: angular.element($document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                    .then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
            };

            vm.showMessenger = function showMessenger(ev){
                $mdDialog.show({
                    controller: ContactDialogController,
                    templateUrl: 'app/components/bottom/contact/contact.dialog.html',
                    parent: angular.element($document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                    .then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                        ev.close();
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                        ev.close();
                    });
            };

            vm.showCopyrights = function showCopyrights(ev){
                $mdDialog.show({
                    controller: CopyrightDialogController,
                    templateUrl: 'app/components/bottom/copyrights/copyrights.dialog.html',
                    parent: angular.element($document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                    .then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
            };

            vm.selectTabPage = function selectTabPage(tabId){
                $rootScope.selectedTab = tabId;
            };
        }
    }

})();
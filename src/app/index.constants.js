/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('trax')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();

(function() {
  'use strict';

  angular
    .module('trax')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController() {

    var vm = this;

    initialize();

    function initialize() {
      vm.init = true;
    }
  }
})();
